/**
 * @file  lab2.c
 * @brief Starter code for lab 2.
 * 
 * @details Modify this file to implement the power meter
 * for lab 2
 * 
 * @author Ned Danyliw
 * @date  09.2015
 */

#include "lab6.h"

// filter alpha and beta for filtering
#define ALPHA 0.9
#define BETA (1 - ALPHA)

 // parameter locations in EE_PROM
#define VOLTS_PER_DIV_ADDRESS 0
#define AMPS_PER_DIV_ADDRESS 1
#define ZERO_VOLTS_ADDRESS 2
#define ZERO_AMPS_ADDRESS 3


// DEFINE SETPOINT HERE; note that this won't be 5V, it'll depend on our voltage divider values
// Currently set to 1.5, which will be 0 since this is a differential ADC



// calibration parameters
uint16_t volts_per_div;
uint16_t amps_per_div;
uint16_t zero_volts;
uint16_t zero_amps;

//current readings
__IO uint16_t voltage_reading;


// Control Variables
#define SETPOINT (volts_per_div * 5)
volatile float PWM = 0.5;
//volatile float prev_volt_meas = 0;
__IO int volt_meas;
__IO int err;
__IO int deriv_err;
__IO int last_err;



/*
volatile float volt_sum = 0.0;
volatile float curr_sum = 0.0;
volatile uint16_t num_cycles = 0;
*/



/**
 * @brief Calibrate out the 0V and 0A offsets
 * @details Reads the ADC value at 0V and 0A and records
 * their offsets in the EEPROM
 */
void calibrate_offset() {
  zero_volts = voltage_reading;

  //Store values in EEPROM
  EE_WriteVariable(ZERO_VOLTS_ADDRESS, zero_volts);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", voltage_reading);
  printf(buffer);
}


/**
 * @brief Updates calibration for the standard voltage
 * @details Calculates the calibration value read from the ADC
 * and stores the result in the EEPROM
 */
void calibrate_voltage() {
  //Code to calculate volts_per_div
  volts_per_div = (voltage_reading - zero_volts) / CAL_VOLTS;

  //Store values in EEPROM
  EE_WriteVariable(VOLTS_PER_DIV_ADDRESS, volts_per_div);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", volts_per_div);
  printf(buffer);
}




/**
 * @brief Initialize the energy meter
 * @details Reads the calibration values from the EEPROM
 */
void meter_init() {
  //Read in calibration constants from EEPROM
  EE_ReadVariable(ZERO_VOLTS_ADDRESS, &zero_volts);
  EE_ReadVariable(VOLTS_PER_DIV_ADDRESS, &volts_per_div);
}


/**
 * @brief Displays energy meter data
 * @details Replace with code to update the display with
 * your own
 */
void meter_display() {
  // Add setpoint to screen

  char setpoint_buf[30];
  sprintf(setpoint_buf, "Setpoint: %.2f", SETPOINT);
  lcd_goto(0,0);
  lcd_puts(setpoint_buf);

  // float volt_meas = ((float)voltage_reading - (float)zero_volts) / (float)volts_per_div;
  // volt_meas = (ALPHA * prev_volt_meas) + (BETA * volt_meas);
  // prev_volt_meas = volt_meas;

  // float err  = SETPOINT - volt_meas;
  // //deriv = (deriv_err - last_err) / (DERIV_TIME * 0.001); // deriv_time is in ms, but just means make D larger
  // float new_PWM = PWM + 0.009*err;// + 0.000015*deriv; 
  // if(new_PWM > 0.95) {
  //   PWM = 0.95;
  // } else if(new_PWM < 0.1) { 
  //   PWM = 0.1;
  // } else PWM = new_PWM;


  // pwm_set(PWM_CHAN1, PWM);

  // // Print to screen
  // char vol_print[20];
  // char PWM_print[20];
  // snprintf(vol_print, sizeof(vol_print), "Voltage: %.3f V        ", volt_meas);
  // snprintf(PWM_print, sizeof(PWM_print), "PWM level: %.4f     ", PWM);

  // lcd_goto(0,1);
  // lcd_puts(vol_print);

  // lcd_goto(0,2);
  // lcd_puts(PWM_print);

  // // Print to serial
  // char vol_meas_print[30];
  // snprintf(vol_meas_print, sizeof(vol_meas_print), "Voltage:  %u ", voltage_reading);
  // printf(vol_meas_print);
  // printf("\n\n\n");

  char err_buff[30];
  char setpoint_buff[30];
  char voltage_reading_buff[30];
  char pwm_buff[30];
  snprintf(err_buff, sizeof(err_buff), "Error:  %i \n", err);
  snprintf(setpoint_buff, sizeof(setpoint_buff), "Setpoint:  %u \n", SETPOINT);
  snprintf(voltage_reading_buff, sizeof(voltage_reading_buff), "Voltage Reading:  %u \n", volt_meas);
  snprintf(pwm_buff, sizeof(pwm_buff), "PWM:  %0.2f \n", PWM);
  printf(err_buff);
  printf(setpoint_buff);
  printf(voltage_reading_buff);
  printf(pwm_buff);
  lcd_goto(0,1);
  lcd_puts(pwm_buff);

}


/**
 * @brief Callback at end of ADC conversion
 * @details Called at the end of the ADC conversions
 */
void my_adc_callback(uint16_t *data) {
  voltage_reading = (uint16_t) data[0];
  volt_meas = (voltage_reading - zero_volts);
  //prev_volt_meas = (ALPHA * prev_volt_meas) + (BETA * volt_meas);
  err = SETPOINT - volt_meas; 
  deriv_err = (err - last_err) * 50000; // deriv_time is in ms, but just means make D larger
  last_err = err;
  PWM = PWM + 0.0001*(float)err + 0.00000001*(float)deriv_err;
  PWM = (PWM > .95) ? .95 : ((PWM < .05) ? .05 : PWM); 
  pwm_set(PWM_CHAN1, PWM);
}
