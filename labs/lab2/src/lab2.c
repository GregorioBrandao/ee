/**
 * @file  lab2.c
 * @brief Starter code for lab 2.
 * 
 * @details Modify this file to implement the power meter
 * for lab 2
 * 
 * @author Ned Danyliw
 * @date  09.2015
 */

#include "lab2.h"

// filter alpha and beta for filtering
#define ALPHA 0.75
#define BETA (1 - ALPHA)

 // parameter locations in EE_PROM
#define VOLTS_PER_DIV_ADDRESS 0
#define AMPS_PER_DIV_ADDRESS 1
#define ZERO_VOLTS_ADDRESS 2
#define ZERO_AMPS_ADDRESS 3

// calibration parameters
uint16_t volts_per_div;
uint16_t amps_per_div;
uint16_t zero_volts;
uint16_t zero_amps;

//current readings
__IO uint16_t voltage_reading;
__IO uint16_t current_reading;

//previous readings for filtering
float prev_curr_meas = 0;
float prev_volt_meas = 0;

//keep track of energy
float energy = 0;


/**
 * @brief Calibrate out the 0V and 0A offsets
 * @details Reads the ADC value at 0V and 0A and records
 * their offsets in the EEPROM
 */
void calibrate_offset() {
  zero_volts = voltage_reading;
  zero_amps = current_reading;

  //Store values in EEPROM
  EE_WriteVariable(ZERO_VOLTS_ADDRESS, zero_volts);
  EE_WriteVariable(ZERO_AMPS_ADDRESS, zero_amps);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", current_reading);
  printf(buffer);
}


/**
 * @brief Updates calibration for the standard voltage
 * @details Calculates the calibration value read from the ADC
 * and stores the result in the EEPROM
 */
void calibrate_voltage() {
  //Code to calculate volts_per_div
  volts_per_div = (voltage_reading - zero_volts) / CAL_VOLTS;

  //Store values in EEPROM
  EE_WriteVariable(VOLTS_PER_DIV_ADDRESS, volts_per_div);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", volts_per_div);
  printf(buffer);
}


/**
 * @brief Updates calibration for the standard current
 * @details Calculates the calibration value read from the ADC
 * and stores the result in the EEPROM
 */
void calibrate_current() {
  //Code to calculate amps_per_div
  amps_per_div = (zero_amps - current_reading) / CAL_CURR;

  //Store values in EEPROM
  EE_WriteVariable(AMPS_PER_DIV_ADDRESS, amps_per_div);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", amps_per_div);
  printf(buffer);


}


/**
 * @brief Initialize the energy meter
 * @details Reads the calibration values from the EEPROM
 */
void meter_init() {
  //Read in calibration constants from EEPROM
  EE_ReadVariable(ZERO_AMPS_ADDRESS, &zero_amps);
  EE_ReadVariable(ZERO_VOLTS_ADDRESS, &zero_volts);
  EE_ReadVariable(AMPS_PER_DIV_ADDRESS, &amps_per_div);
  EE_ReadVariable(VOLTS_PER_DIV_ADDRESS, &volts_per_div);
}


/**
 * @brief Displays energy meter data
 * @details Replace with code to update the display with
 * your own
 */
void meter_display() {
  float curr_meas = ((float)zero_amps - (float)current_reading) / (float)amps_per_div;
  float volt_meas = ((float)voltage_reading - (float)zero_volts) / (float)volts_per_div;

  // char buffer[100];
  // snprintf(buffer, sizeof(buffer), "Current Reading: %u\n Zero Amps: %u\n Amps Per Div: %u\n Curr Meas: %u\n", current_reading, zero_amps, amps_per_div, curr_meas);
  // printf(buffer);

  curr_meas = (ALPHA * prev_curr_meas) + (BETA * curr_meas);
  volt_meas = (ALPHA * prev_volt_meas) + (BETA * volt_meas);
  prev_curr_meas = curr_meas;
  prev_volt_meas = volt_meas;

  float power = curr_meas * volt_meas;
  energy += power * .05;

  char curr_print[20];
  char vol_print[20];
  char pow_print[20];
  char ener_print[20];
  snprintf(curr_print, sizeof(curr_print), "Current: %.1f A   ", curr_meas);
  snprintf(vol_print, sizeof(vol_print), "Voltage: %.1f V     ", volt_meas);
  snprintf(pow_print, sizeof(pow_print), "Power: %.1f W     ", power);
  snprintf(ener_print, sizeof(ener_print), "Energy: %.1f J     ", energy);


  lcd_goto(0,0);
  lcd_puts(vol_print);
  lcd_goto(0,1);
  lcd_puts(curr_print);
  lcd_goto(0,2);
  lcd_puts(pow_print);
  lcd_goto(0,3);
  lcd_puts(ener_print);

  //Code here
}


/**
 * @brief Callback at end of ADC conversion
 * @details Called at the end of the ADC conversions
 */
void my_adc_callback(uint16_t *data) {
  voltage_reading = (uint16_t) data[0];
  current_reading = (uint16_t) data[1];
}
