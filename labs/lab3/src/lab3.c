/**
 * @file  lab2.c
 * @brief Starter code for lab 2.
 * 
 * @details Modify this file to implement the power meter
 * for lab 2
 * 
 * @author Ned Danyliw
 * @date  09.2015
 */

#include "lab3.h"

// filter alpha and beta for filtering
#define ALPHA 0.9
#define BETA (1 - ALPHA)

 // parameter locations in EE_PROM
#define VOLTS_PER_DIV_ADDRESS 0
#define AMPS_PER_DIV_ADDRESS 1
#define ZERO_VOLTS_ADDRESS 2
#define ZERO_AMPS_ADDRESS 3


// calibration parameters
uint16_t volts_per_div;
uint16_t amps_per_div;
uint16_t zero_volts;
uint16_t zero_amps;

//current readings
__IO uint16_t voltage_reading;
__IO uint16_t current_reading;

//previous readings for filtering
float prev_curr_meas = 0;
float prev_volt_meas = 0;

//keep track of energy
float energy = 0;

// MPPT Constants
float duty_factor = 0.5;
float ddf = 0.03;

volatile float volt_sum = 0.0;
volatile float curr_sum = 0.0;
volatile uint16_t num_cycles = 0;




/**
 * @brief Calibrate out the 0V and 0A offsets
 * @details Reads the ADC value at 0V and 0A and records
 * their offsets in the EEPROM
 */
void calibrate_offset() {
  zero_volts = voltage_reading;
  zero_amps = current_reading;

  //Store values in EEPROM
  EE_WriteVariable(ZERO_VOLTS_ADDRESS, zero_volts);
  EE_WriteVariable(ZERO_AMPS_ADDRESS, zero_amps);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", current_reading);
  printf(buffer);
}


/**
 * @brief Updates calibration for the standard voltage
 * @details Calculates the calibration value read from the ADC
 * and stores the result in the EEPROM
 */
void calibrate_voltage() {
  //Code to calculate volts_per_div
  volts_per_div = (voltage_reading - zero_volts) / CAL_VOLTS;

  //Store values in EEPROM
  EE_WriteVariable(VOLTS_PER_DIV_ADDRESS, volts_per_div);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", volts_per_div);
  printf(buffer);
}


/**
 * @brief Updates calibration for the standard current
 * @details Calculates the calibration value read from the ADC
 * and stores the result in the EEPROM
 */
void calibrate_current() {
  //Code to calculate amps_per_div
  amps_per_div = (current_reading - zero_amps) / CAL_CURR;

  //Store values in EEPROM
  EE_WriteVariable(AMPS_PER_DIV_ADDRESS, amps_per_div);

  char buffer[100];
  snprintf(buffer, sizeof(buffer), "%u\n", amps_per_div);
  printf(buffer);


}


/**
 * @brief Initialize the energy meter
 * @details Reads the calibration values from the EEPROM
 */
void meter_init() {
  //Read in calibration constants from EEPROM
  EE_ReadVariable(ZERO_AMPS_ADDRESS, &zero_amps);
  EE_ReadVariable(ZERO_VOLTS_ADDRESS, &zero_volts);
  EE_ReadVariable(AMPS_PER_DIV_ADDRESS, &amps_per_div);
  EE_ReadVariable(VOLTS_PER_DIV_ADDRESS, &volts_per_div);
}


/**
 * @brief Displays energy meter data
 * @details Replace with code to update the display with
 * your own
 */
void meter_display() {
  float avg_curr = curr_sum / num_cycles;
  float avg_volt = volt_sum / num_cycles;

  float old_power = prev_curr_meas * prev_volt_meas;
  prev_curr_meas = avg_curr;
  prev_volt_meas = avg_volt;

  float power = avg_curr * avg_volt;
  //energy += power * .05;
  float df_old = duty_factor;

  if(power >= old_power) {
    old_power = power;
    df_old = duty_factor;
    duty_factor += ddf;
    if (duty_factor < 0) {
      duty_factor = 0;
      ddf = -ddf;
    } else if(duty_factor > 1) {
      duty_factor = 1;
      ddf = -ddf;
    }
  } else {
    ddf = -ddf;
    duty_factor = df_old;
    old_power = 0.0;
  }

  pwm_set(PWM_CHAN1, duty_factor);


  float curr_meas = (avg_curr - (float)zero_amps) / (float)amps_per_div;
  float volt_meas = (avg_volt - (float)zero_volts) / (float)volts_per_div;

  volt_sum = 0;
  curr_sum = 0;
  num_cycles = 0;
  // char buffer[100];
  // snprintf(buffer, sizeof(buffer), "Current Reading: %u\n Zero Amps: %u\n Amps Per Div: %u\n Curr Meas: %u\n", current_reading, zero_amps, amps_per_div, curr_meas);
  // printf(buffer);



  char cal_vals[100];
  snprintf(cal_vals, sizeof(cal_vals), "Zero Amps: %u, Zero Volts: %u, \n Amp/div: %u, Volt/div: %u \n", 
    zero_amps, zero_volts, amps_per_div, volts_per_div);
  printf(cal_vals);




  /*
  printf("\n");
  char blank_space[30];
  printf(blank_space);
  char curr_meas_print[30];
  snprintf(curr_meas_print, sizeof(curr_meas_print), "Current:  %u,    ", current_reading);
  printf(curr_meas_print);
  char vol_meas_print[30];
  snprintf(vol_meas_print, sizeof(vol_meas_print), "Voltage:  %u ", voltage_reading);
  printf(vol_meas_print);
  printf("\n\n\n");
  */

  //curr_meas = (ALPHA * prev_curr_meas) + (BETA * curr_meas);
  //volt_meas = (ALPHA * prev_volt_meas) + (BETA * volt_meas);




  char curr_print[20];
  char vol_print[20];
  char pow_print[20];
  char ener_print[20];
  snprintf(curr_print, sizeof(curr_print), "Current: %.1f A   ", curr_meas);
  snprintf(vol_print, sizeof(vol_print), "Voltage: %.1f V     ", volt_meas);
  snprintf(pow_print, sizeof(pow_print), "Power: %.1f W     ", curr_meas * volt_meas);
  snprintf(ener_print, sizeof(ener_print), "DF: %.2f      ", duty_factor);

  printf(curr_print);
  printf(vol_print);
  printf(pow_print);
  printf(ener_print);
  printf("\n");


  lcd_goto(0,0);
  lcd_puts(vol_print);
  lcd_goto(0,1);
  lcd_puts(curr_print);
  lcd_goto(0,2);
  lcd_puts(pow_print);
  lcd_goto(0,3);
  lcd_puts(ener_print);



  //Code here
}


/**
 * @brief Callback at end of ADC conversion
 * @details Called at the end of the ADC conversions
 */
void my_adc_callback(uint16_t *data) {
  voltage_reading = (uint16_t) data[0];
  current_reading = (uint16_t) data[1];
  volt_sum += voltage_reading;
  curr_sum += current_reading;
  num_cycles = num_cycles + 1;
}
