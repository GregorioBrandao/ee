/**
 * @file  main.c
 * @brief Starter code for Lab 5
 * 
 * @details Measures the frequency of the signal on PB10 and prints it
 * to the LCD
 * 
 * @author Ned Danyliw
 * @date  10.2015
 */
#include "ge_libs.h"
#include <string.h>
#include <stdlib.h>

#define P pow(10.0, -3)
#define D pow(10.0, -2)
#define DERIV_TIME 2.0 // in ms

float deriv_err = 0;
float last_err = 0;
float err = 0;


// Updates errors to be used in calculating the derivative. 
void compute_deriv() {
  last_err = deriv_err;
  deriv_err = err;

}

/* Clamps PWM if it's outside the possible ranges
 */
float clamp_PWM(float PWM) {
  if(PWM > 1.0) {
    PWM = 1.0;
  } else if(PWM < 0.0) { // Check this syntax
    PWM = 0.0;
  }
  return PWM;
}

int compare_function(const void* a, const void* b){
  return (*(float*)a - *(float*)b);
}


/* ----------------- GPIO INITIALIZATION ---------------*/
void setup_buttons() {
  gpio_setup_pin(GE_PBTN1, GPIO_INPUT, false, false);
  gpio_setup_pin(GE_PBTN2, GPIO_INPUT, false, false);
  gpio_setup_pin(GE_PBTN3, GPIO_INPUT, false, false);
  gpio_setup_pin(GE_PBTN4, GPIO_INPUT, false, false);
}


/**
  * @brief  Main program.
  * @param  None 
  * @retval None
  */
int main(void)
{  
  //Initialize library
  ge_init();
  setup_buttons();

  timer_id_t deriv_tim = timer_register(DERIV_TIME, &compute_deriv, GE_PERIODIC);
  timer_start(deriv_tim);

  // Set up PWM Library
  // CHECK WHETHER THIS FREQUENCY IS GOOD!!!!!!!
  pwm_freq(100000); // Frequency of pulses; duty cycle determines how long high
  pwm_set_pin(GE_PWM1);

  // Set maximum capture frequency
  ic_int_set_maxf(600.0);

  uint8_t filter_number = 11;

  float speed_buf[filter_number];
  float sorted_buf[filter_number];

  float filtered_speed = 0;

  uint8_t counter = 0;
  uint8_t index = 0;

  // Variables for controller
  float PWM = 0;
  float setpoint = 0;
  float deriv = 0;

	lcd_goto(0,0);
	lcd_puts("++");
	lcd_goto(0,3);
	lcd_puts("--");
	lcd_goto(19,0);
	lcd_puts("+");
	lcd_goto(19,3);
	lcd_puts("-");

  while (1) {


     // Allow setpoint to be set. 
    if(!gpio_read_pin(GE_PBTN3)) {
      setpoint += 20;
      while(!gpio_read_pin(GE_PBTN3));
    } else if(!gpio_read_pin(GE_PBTN4)) {
      setpoint -= 20;
      while(!gpio_read_pin(GE_PBTN4));
    }
    

    
    // Step the setpoint
    if (!gpio_read_pin(GE_PBTN1)) {
        setpoint += 150;
	while (!gpio_read_pin(GE_PBTN1));
    }
    if (!gpio_read_pin(GE_PBTN2)) {
	setpoint -= 150;
	while (!gpio_read_pin(GE_PBTN2));
    }

    if(setpoint < 0) {
      setpoint = 0;
    }

    if (index>filter_number-1){
      index = 0;
    }
    float temp = ic_int_read_freq();
    speed_buf[index]=temp;
    index++;

    memcpy(sorted_buf, speed_buf, filter_number*sizeof(float)); //make copy so we can sort in place
    qsort(sorted_buf, filter_number, sizeof(float), compare_function);

    filtered_speed = sorted_buf[filter_number/2+1];

    if (counter % 20 == 0){
      counter = 0;
      char buf[30];
      char setpoint_buf[30];
      char pwm_buf[30];
      //lcd_clear();
      sprintf(buf, "Speed: %03.f", filtered_speed);
      sprintf(setpoint_buf, "Setpoint: %03.f", setpoint);
      sprintf(pwm_buf, "PWM: %0.2f", PWM);
      lcd_goto(4, 0);
      lcd_puts(buf);

      lcd_goto(4,1);
      lcd_puts(setpoint_buf);

      lcd_goto(4,2);
      lcd_puts(pwm_buf);
    }



    // ADD CONTROLLER CODE HERE
    /********* Controller Code **************/

    // Update PWM using PID values
    err  = setpoint - filtered_speed;
    deriv = (deriv_err - last_err) / (DERIV_TIME * 0.001); // deriv_time is in ms, but just means make D larger
    float new_PWM = PWM + 0.00009*err;// + 0.000015*deriv; // Make sure to include math.h
    if(new_PWM > 0.95) {
      PWM = 0.95;
      } else if(new_PWM < 0.0) { 
      PWM = 0.0;
      } else PWM = new_PWM;

    //PWM = 1.0 - PWM;

    //PWM = 0.5;

    pwm_set(PWM_CHAN1, PWM);

    /********* End of Controller Code *******/

    delay_ms(10);
    counter++;
  }
}



#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif